resource "azurerm_resource_group" "lab02-rg" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_virtual_network" "lab1-vnet" {
  name                = var.virtual_net_name
  location            = azurerm_resource_group.lab02-rg.location
  resource_group_name = azurerm_resource_group.lab02-rg.name
  address_space       = var.virtual_net_address_space
  dns_servers         = ["10.0.0.4", "10.0.0.5"]
}

resource "azurerm_subnet" "lab02-subnet1" {
  name                 = var.subnet01_name
  resource_group_name  = azurerm_resource_group.lab02-rg.name
  virtual_network_name = azurerm_virtual_network.lab1-vnet.name
  address_prefixes     = var.subnet01_address_space
}

resource "azurerm_subnet" "lab02-subnet2" {
  name                 = var.subnet02_name
  resource_group_name  = azurerm_resource_group.lab02-rg.name
  virtual_network_name = azurerm_virtual_network.lab1-vnet.name
  address_prefixes     = var.subnet02_address_space
}

resource "azurerm_network_security_group" "lab02-nsg" {
  name                = var.network_security_group_01_name
  resource_group_name = azurerm_resource_group.lab02-rg.name
  location            = azurerm_resource_group.lab02-rg.location

  security_rule {
    name                       = "rule1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_network_security_group" "lab02-nsg2" {
  name                = var.network_security_group_02_name
  resource_group_name = azurerm_resource_group.lab02-rg.name
  location            = azurerm_resource_group.lab02-rg.location

  security_rule {
    name                       = "rule1"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "lab02-snsga" {
  subnet_id                 = azurerm_subnet.lab02-subnet1.id
  network_security_group_id = azurerm_network_security_group.lab02-nsg.id
}

resource "azurerm_subnet_network_security_group_association" "lab02-snsga2" {
  subnet_id                 = azurerm_subnet.lab02-subnet2.id
  network_security_group_id = azurerm_network_security_group.lab02-nsg2.id
}
