variable "az_linux_name" {
  type    = string
  default = "lab02s2-db1-u-vm1"
}

variable "az_linux_resource_group_name" {
  type    = string
  default = "lab02-rg"
}

variable "az_linux_location" {
  type    = string
  default = "Canada Central"
}

variable "az_linux_size" {
  type    = string
  default = "Standard_B1s"
}

variable "az_linux_admin_username" {
  type    = string
  default = "auto"
}

variable "az_linux_network_interface_ids" {
  type = list(any)
  default = [

  ]
}

variable "az_linux_admin_ssh_key" {
  type = map(any)
  default = {
    username   = "auto"
    public_key = "/home/auto/.ssh/id_rsa.pub"
  }
}

variable "az_linux_os_disk" {
  type = map(any)
  default = {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
    disk_size_gb         = 32
  }
}

variable "az_linux_source_image_reference" {
  type = map(any)
  default = {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "19.04"
    version   = "latest"
  }
}